

rovSoftware = softwareSystem "ROV Software" "ROV software (ROS)" {

    #rovControl = container "ROV Control Arbitration"
    
    perceptionDrivers = container "Perception Drivers" {
        navDrivers = component "Navigation Sensor Abstraction" "Integrates external 'navigation' estimates and emulates a real nav sensor"

        tactileSensingDriver = component "Tactile Sensing Driver"

        stereo = component "Stereo Camera Driver" "Depth mapping built-in to stereo system."
        color = component "Color Camera Driver"

        forceTorque = component "Force-Torque Driver"

    }

    group "Perception Processing" {

        depthMap = container "Depth map / point cloud generation" "ACTIVE RESEARCH: Turbidity-aware depth map generation" {
            dlDepthMap = component "DL-based depth-map algorithm"
        }
        
        objId = container "Object Identification / Segmentation" {
            imgObjectDetection = component "Image-based object identification"
            pcObjectDetection = component "Point-Cloud-based object identification"

            segmentation = component "Segmentation" "Image, depth image, point cloud"

            imgObjectDetection -> segmentation
            pcObjectDetection -> segmentation
        }

        reconstruction = container "Reconstruction/SLAM" "ACTIVE RESEARCH: Opti-acoustic SLAM" {
            objectRecon = component "Object Reconstruction"
            worldRecon = component "World Reconstruction"

            localization = component "Localization / Pose tracking"

            obstacleChecker = component "Obstacle Checking"

            worldRecon -> obstacleChecker
            worldRecon -> localization
        }

        depthMap -> objId

        objId -> reconstruction

        ## Container-level links
        perceptionDrivers -> depthMap "Raw perception products"
        perceptionDrivers -> dlDepthMap "Stereo imagery, CameraInfo"

        perceptionDrivers -> objId "Raw perception products"
        perceptionDrivers -> reconstruction "Raw perception products"

        ## Component-to-component links
        stereo -> depthMap "/../trisect/{left,right}/image_raw" "sensor_msgs.Image"
        
        color -> imgObjectDetection "/../trisect/color/image_raw" "sensor_msgs.Image"
        stereo -> pcObjectDetection
        depthMap -> pcObjectDetection


        depthMap -> reconstruction "Depth map"
        dlDepthMap -> reconstruction "Depth map"
        depthMap -> worldRecon "/../trisect/point_cloud" "sensor_msgs.PointCloud2"
        navDrivers -> worldRecon "IMU hint"

        forceTorque -> objectRecon "/../force_torque/wrench" "geometry_msgs.WrenchStamped"


        tactileSensingDriver -> objectRecon "/../tactile_sensor/point_cloud" "sensor_msgs.PointCloud2"

        color -> worldRecon "/../trisect/color/image_raw" "sensor_msgs.Image"


        color -> objectRecon "/../trisect/color/image_raw" "sensor_msgs.Image"
        depthMap -> objectRecon "/../trisect/point_cloud" "sensor_msgs.PointCloud2"
        navDrivers -> objectRecon ""

    }



    group "Planning" {
        pathPlanning = container "Path Planning and Trajectory Optimization" "ACTIVE RESEARCH: Dongsik" {

            pathPlanningOne = component "Path Planning Element One" "ACTIVE RESEARCH: Dongsik"
            pathPlanningTwo = component "Path Planning Element Two" "ACTIVE RESEARCH: Dongsik"

            pathPlanningOne -> PathPlanningTwo "Something"

        }

        graspSynthesis = container "Grasp Planning Node" "ACTIVE RESEARCH: Tim" {

            graspPlanningOne = component "Grasp Planning Element One" "ACTIVE RESEARCH: Tim" 

            graspPlanningTwo = component "Grasp Planning Element Two" "ACTIVE RESEARCH: Tim"

            graspPlanningOne -> graspPlanningTwo "Something"

        } 
    }

    segmentation -> graspSynthesis "(segmented?) depth maps"
    segmentation -> graspPlanningOne "Depth maps of object"

    reconstruction -> pathPlanning "World model"


    group "Control" {

        armControl = container "Arm Control Interface"
        vehicleControl = container "Vehicle Control Requests"

        bodyControl = container "Whole Body Control" "/control/whole_body_control" "ROS Node"
    
        armControl -> bodyControl
        vehicleControl -> bodyControl
    }

    pathPlanningTwo -> vehicleControl 

    graspPlanningTwo -> armControl



    reconstruction -> bodyControl "Position and obstacle info"
    localization -> bodyControl
    obstacleChecker -> bodyControl

    ui = container "User Interface"

    group "Decision System Support" {
        dss = container "Decision System Support"

    }



    depthMap -> ui "Depth maps"
    reconstruction -> ui "Reconstructions"
    perceptionDrivers -> ui "Raw data"


    #objectRecon -> dssDashboard "/../object_point_cloud"
    #worldRecon -> dssDashboard "/../world_point_cloud"
    #localization -> dssDashboard "/../vehicle_post_estimate" "geometry_msgs.PoseStamped"
    #obstacleChecker -> dssDashboard "/../obstacles" "...?"

    #planning -> dss "Path and grasp commands"

    pathPlanning -> dss "/plan/traj_serv" "actionlib srv"
    graspSynthesis -> dss "/plan/grasp_serv" "actionlib srv"

    dss -> bodyControl "Action requests"
    #dssDashboard -> bodyControl "/control/goto_waypoint" "actionlib srv"

}
