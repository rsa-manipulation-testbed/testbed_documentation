# Running the testbed

## Running the driver

The ROS testbed driver runs as a single node.   To run the software:

1. {ref}`Install the software using catkin <installation>` and add it to your workspace:

```
cd testbed_ws
catkin build
source devel/setup.bash
```

1. Ensure the `TESTBED_SITE` environment variable is set and that there's a corresponding `${TESTBED_SITE}_config.yaml` which gives the Clearpath motor serial numbers.

1. Run the testbed launchfile:

```
roslaunch testbed_driver testbed.launch
```

By default the driver runs all four axes but individual axes can be disable if they are not available:

```
roslaunch testbed_driver testbed.launch enable_yaw:=false
```


## Teleoperation mode

A simple [joystick-based remote control node](https://gitlab.com/rsa-manipulation-testbed/testbed_hw/-/tree/main/testbed_teleop) is provided.  Right now we are using Logitech joysticks, the axis mapping may be different on XBox controllers.

1. In a separate window, source the Testbed workspace:

```
source devel/setup.bash
```

1. Run the teleop node:

```
roslaunch testbed_teleop teleop.launch
```

When done, ctrl-C to quit.


In the current version, all four axes start in a velocity control mode, with all axes disabled.

The left shoulder button enables all axes.  The right should button disables all axes.

The left joystick controls CoreXY.  The right joystick controls yaw (left-right) and Z-axis (up-down).

Pressing start switches the system to position control mode.  This mode is highly untested and will cause the testbed to immediately try to return to zero position ... do not use.   Pressing start again will put the unit in velocity mode.


## Zeroing the system
