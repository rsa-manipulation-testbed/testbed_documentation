# Configuration

Most system configuration is centralized in the [`testbed_sw`](https://gitlab.com/rsa-manipulation-testbed/testbed_sw) repository.   This repo contains multiple sub-packages:

* [`testbed_config`](https://gitlab.com/rsa-manipulation-testbed/testbed_sw/-/tree/main/testbed_config) contains testbed hardware and controller configurations.
* [`testbed_description/`](https://gitlab.com/rsa-manipulation-testbed/testbed_sw/-/tree/main/testbed_description/) contains the URDF and meshes for the testbed.
* [`testbed_moveit_config/`](https://gitlab.com/rsa-manipulation-testbed/testbed_sw/-/tree/main/testbed_moveit_config/) contains configuration for MoveIt! and Gazebo for the testbed.

Most of the configuration should be set to appropriate default values, with the exception of the Clearpath motor mapping, which must be set for each Testbed site.

# Motor mapping

The software must be told explicitly which Clearpath motors correspond to which testbed axes.  This is done through a config file in [`testbed_config`](https://gitlab.com/rsa-manipulation-testbed/testbed_sw/-/tree/main/testbed_config/config) called `{TESTBED_SITE}_config.yaml` where `{TESTBED_SITE}` is a descriptive name for a given testbed installation.

The file contains four config params which give the Clearpath serial number fo the for axis.  For example [`uwapl_config.yaml`]() is:

```
#
# Clearpath motor serial number for APL Testbed
#
yaw_sn: 70403992
zaxis_sn: 73003302
corexy_a_sn: 65800963
corexy_b_sn: 66102023
```


The [`testbed.launch`](https://gitlab.com/rsa-manipulation-testbed/testbed_hw/-/blob/main/testbed_driver/launch/testbed.launch) assumes that the argument `testbed_site` is set, or as a default will look for an environment variable `TESTBED_SITE`

!!! We strongly suggest that the environment variable `TESTBED_SITE` be set automatically on the user account/computer which drives the testbed.


# Clearpath motor configuration

The [`clearpath_config.yaml`](https://gitlab.com/rsa-manipulation-testbed/testbed_sw/-/blob/main/testbed_config/config/clearpath_config.yaml) file sets motor paramters **within the Clearpath motor itself**.  This includes velocity, acceleration, current and jerk limits.  As these limits are set in the motors they are **absolute** limits  on motor performance.

# ROS Controller configuration and tuning

The [`testbed_controllers.yaml`](https://gitlab.com/rsa-manipulation-testbed/testbed_sw/-/blob/main/testbed_config/config/testbed_controllers.yaml) file describes the motor control interfaces exposed by the Testbed to the ROS Control system.  Since the testbed uses position control loops which run **within ROS** this includes PID parameters for those control loops, as well as joint limits to be enforced by the software controllers.