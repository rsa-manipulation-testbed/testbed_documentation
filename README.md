> **NOTE:** This site is slowly being migrated to  [wave_documentation](https://gitlab.com/rsa-manipulation-testbed/wave-documentation).

Generally, updates should be made there.


# Documentation and architecture for the RSA Manipulation Testbed

This is the system documentation for the RSA Manipulation tested.  The latest published version is served via Gitlab Pages [here](https://rsa-manipulation-testbed.gitlab.io/testbed_testbed/).

The test documentation is written in [Markdown](https://www.markdownguide.org/) in the [Sphinx](https://www.sphinx-doc.org/en/master/) environment using the [Read the Docs](https://readthedocs.org/) theme.  These files have the `md` extension and include the C4Model images.

There is also some system architecture documentation written using the [C4Model](https://c4model.com/), and is stored in two formats, software diagrams are stored in the [Structurizr DSL](https://structurizr.com/) with the extension `.structurizr`.    These files are converted to graphics using a combination of the [structurizr CLI](https://github.com/structurizr/cli) and the [PlantUML](https://plantuml.com) renderer.


**The Structurizr source files are in `diagrams`**

**The Sphinx source files are in `docs`**

# I want to ..

## Build everything

The steps for building the documentation are encoded in the `Makefile`.   In general, `make` will build the diagrams and documentation, but you must have [Docker](https://www.docker.com) installed.  The build process uses three Docker images in sequence:  the first runs Structurizr, the second runs PlantUML, and the third runs Sphinx.

`make` should do everything.  It may take a bit the first time through.

The resulting files will be in the `public/` directory.   Point your web browser to `public/index.html`

## Update just a diagram

Diagrams are updated by editing the `.structurizr` files in the `diagrams/` directory.  Here are some links for understanding the C4Model:

* [Main C4 Model page](https://c4model.com/)

The full Structurizr DSL reference is [here](https://github.com/structurizr/dsl/blob/master/docs/language-reference.md).

`make c4model` will then build only the diagrams, outputing them into `_static/models/`

## Update just the web site

The web site is contained in the Markdown `.md` files in the `docs/` directory..   After editing, `make sphinx` will rebuild the website (but not the diagrams).



